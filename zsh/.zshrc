# Path to your oh-my-zsh installation.
export ZSH="/$HOME/.oh-my-zsh"

ZSH_THEME="lambda"

DISABLE_AUTO_UPDATE="true"
DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="true"

COMPLETION_WAITING_DOTS="true"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration
export DEFAULT_USER=jens

# You may need to manually set your language environment
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

if hash nvim 2> /dev/null ; then
  export EDITOR='nvim'
else 
  export EDITOR='vim'
fi

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
if hash thefuck 2> /dev/null ; then
	eval $(thefuck --alias fuck)
fi

BULLETTRAIN_PROMPT_ORDER=(
	status
	dir
	git
	)

export PKG_CONFIG_PATH="/usr/local/opt/openssl/lib/pkgconfig"
if hash jenv 2> /dev/null ; then
	export PATH="$HOME/.jenv/bin:$PATH"
	eval "$(jenv init -)"
fi

if [ "$(uname 2> /dev/null)" = "Darwin" ]; then
	export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
	export PATH="/Users/jens/Library/Python/3.7/bin:$PATH"
	export PYTHON_SITE_PACKAGES="/usr/local/lib/python3.7/site-packages"
	alias lock="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"
fi

if [ "$(uname 2> /dev/null)" = "Linux" ]; then
	export PYTHON_SITE_PACKAGES="/usr/lib/python3.7/site-packages"
	export PATH="$HOME/.local/bin:$PATH"
fi
export PATH="/usr/local/sbin:$PATH"

uzip() { unzip $1 && rm $1 }

export VAGRANT_HOME=/Volumes/Data/vagrant_home
export GPG_TTY=$(tty)
