Dit is mijn dotfile repository. Er zijn vele zoals deze, maar deze is van mij.

Link the desired files into the following places:

- .basrc -> $HOME/.bashrc
- .gitconfig -> $HOME/.gitconfig
- .vimrc -> $HOME/.vimrc
- mutt/ -> $HOME/.mutt
- sway/ -> $HOME/.config/sway
- newsboat -> $HOME/.newsboat
