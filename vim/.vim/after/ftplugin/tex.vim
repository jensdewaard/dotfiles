"nmap <buffer> <leader>m <Plug>(vimtex-compile)
"nmap <buffer> <leader>r <Plug>(vimtex-view)
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-pdf',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
setlocal fo+=t
