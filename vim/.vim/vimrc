set nocompatible
filetype on

" Install missing vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


" Plugins
if has('nvim')
	call plug#begin('~/.local/share/nvim/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

Plug 'arcticicestudio/nord-vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'lervag/vimtex', {'for': 'tex'}
Plug 'fatih/vim-go', {'for': 'go'}
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'let-def/vimbufsync', {'for':'coq'}
Plug 'whonore/coqtail', {'for': 'coq'}
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'rodjek/vim-puppet'
Plug 'elmcast/elm-vim'

call plug#end()

colorscheme default

" Detect file changes from outside vim
set autoread
" Ignore case when searching
set ignorecase
" Show matching brackets
set showmatch
" Show line numbers
set number

" Wrap text at 79 characters
set tw=79

syntax enable
set background=dark
set encoding=utf8

" Use spaces instead of tab
set expandtab
set shiftwidth=4
set tabstop=4
set smarttab

" Always keep lines below and above the curser
set scrolloff=5

set hidden
set ai
set si
set wrap
map j gj
map k gk
" color column 80 red
set cc=80

set splitright " Open vertical splits right of the current split
set splitbelow " Open horitontal splits below the current split

" Set the leader to spacebar
nnoremap <space> <NOP>
let mapleader=" " 


inoremap kj <Esc>
map <Leader>m :Make<CR>

set backspace=indent,eol,start

nmap <leader>sp :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" Let CtrlP ignore files in .gitignore
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" Enable folding
set foldmethod=indent
set foldlevel=99

"Vim Gutter always show signcolumn
if exists('&signcolumn')  " Vim 7.4.2201
  set signcolumn=yes
else
  let g:gitgutter_sign_column_always = 1
endif

" netrw settings
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

function! ToggleVExplorer()
	if exists("t:expl_buf_num")
		let expl_win_num = bufwinnr(t:expl_buf_num)
		if expl_win_num != -1
			let cur_win_nr = winnr()
			exec expl_win_num . 'wincmd w'
			close
			exec cur_win_nr . 'wincmd w'
		endif
		unlet t:expl_buf_num
	else
		exec '1wincmd w'
		Vexplore
		let t:expl_buf_num = bufnr("%")
	endif	
endfunction
"map <silent> <C-n> :call ToggleVExplorer()<CR>

" NERDTree
autocmd StdinReadPre * let s:std_in=1
let g:NERDTreeWinSize=28

let NERDTreeIgnore = [
	\ '\.pyc$',
    \ '\.vo$',
	\ '\.glob$',
	\ '\.aux$',
    \ '\.fdb_latexmk$',
	\ '\.fls$',
	\ '\.out$',
	\ '\.synctex.gz$',
	\ '\.toc$',
	\]

map <C-n> :NERDTreeToggle<CR>

" Close vim if only open window is nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"
" Filetype settings

" VimTex
let g:tex_flavor = 'latex'
let g:vimtex_mappings_enabled = 0
let g:vimtex_disable_version_warning = 1

" Coq
let g:coqtail_nomap=1

" coc.vim
" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

set diffopt-=internal
set diffopt+=algorithm:patience
set diffopt+=indent-heuristic

set wildignore+=node_modules/**
