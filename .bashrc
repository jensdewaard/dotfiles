#
# the .bashrc of Jens de Waard
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias music='ncmpcpp'

# Modify the bashprompt 
# [ username @ hostname current_dir ] 
PS1='[\u@\h \W]\$ '


export VISUAL="vim"
export TERMINAL="kitty"

if [ -f $HOME/.bashrc_platform ]; then
	source $HOME/.bashrc_platform
fi
